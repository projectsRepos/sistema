import { BaseControllerService } from './base.controller.service';
import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root',
})
export class SchoolControllerService extends BaseControllerService {
	protected model = 'school/';
}
