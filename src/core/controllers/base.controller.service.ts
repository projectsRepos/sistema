import { ApiService } from '../api/api.service';
import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root',
})
export class BaseControllerService {

	constructor(private apiService: ApiService) {}

	protected model: string;

	public get(params?): Promise<any> {
		return this.apiService.call('GET', this.model, null, params);
	}
}
