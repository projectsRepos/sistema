import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import ENDPOINTS from '../endpoints';

@Injectable({
	providedIn: 'root'
})
export class ApiService {

	private endpoint = ENDPOINTS.development;

	constructor(private http: HttpClient) { }

	public call(method: 'GET' | 'POST' | 'PUT' | 'DELETE', endpoint: string, requestBody?: any, requestParams?: any): Promise<any> {
		let params = new HttpParams();
		if (requestParams) {
			Object.keys(requestParams).forEach(key => {
				params = params.append(key, requestParams[key]);
			});
		}
		return this.http[method.toLowerCase()](this.endpoint + endpoint, { params, body: requestBody }).toPromise();
	}
}
