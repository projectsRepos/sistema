import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
	selector: 'app-loading',
	templateUrl: './loading.component.html',
	styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit, OnDestroy {

	public isLoading: boolean;
	private loading$: Subscription;

	constructor(private loadingService: LoadingService) {
		this.loading$ = this.loadingService.getLoadingState().subscribe(loadingState => this.isLoading = loadingState);
	}

	ngOnInit() {
	}

	ngOnDestroy() {
		if (this.loading$) {
			this.loading$.unsubscribe();
		}
	}

}
