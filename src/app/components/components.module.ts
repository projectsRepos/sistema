import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuItemComponent } from './menu-item/menu-item.component';
import { LoadingComponent } from './loading/loading.component';
import { ListItemComponent } from './list-item/list-item.component';

const COMPONENTS = [
	MenuItemComponent,
	LoadingComponent,
];

@NgModule({
	declarations: [...COMPONENTS, LoadingComponent, ListItemComponent],
	exports: [...COMPONENTS],
	imports: [
		CommonModule
	]
})
export class ComponentsModule { }
