import { Component, OnInit } from '@angular/core';
import { SchoolControllerService } from 'src/core/controllers/school.controller.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
	selector: 'app-schools',
	templateUrl: './schools.component.html',
	styleUrls: ['./schools.component.scss']
})
export class SchoolsComponent implements OnInit {

	constructor(private schoolController: SchoolControllerService, private loading: LoadingService) { }

	ngOnInit() {
		this.getData();
	}

	public getData(): void {
		this.schoolController.get().then(apiResponse => {
		});
	}

}
