import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	public schoolCount: number;
	public schoolGroupCount: number;
	public studentCount: number;


	constructor() {}

	ngOnInit() {
	}

}
