import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
	private loadingState: BehaviorSubject<boolean>;

	constructor() {
		this.loadingState = new BehaviorSubject(false);
	}

	/**
	 * Retorna como observable o estado de loading do service
	 */
	public getLoadingState(): BehaviorSubject<boolean> {
		return this.loadingState;
	}

	/**
	 * Seta como true o estado de loading do componente
	 */
	public showLoading(): void {
		this.loadingState.next(true);
	}

	/**
	 * Seta como true o estado de loading do componente
	 */
	public hideLoading(): void {
		this.loadingState.next(false);
	}
}
