import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComponentsModule } from './components/components.module';
import { HomeComponent } from './pages/home/home.component';
import { ApiService } from 'src/core/api/api.service';
// import { BaseControllerService } from 'src/core/controllers/base.controller.service';
import { SchoolControllerService } from 'src/core/controllers/school.controller.service';
import { SchoolsComponent } from './pages/schools/schools.component';
import { LoadingService } from './services/loading.service';

const PAGES = [
	SchoolsComponent,
	HomeComponent,
];

const PROVIDERS = [
	ApiService,
	// BaseControllerService,
	SchoolControllerService,
	LoadingService,
];

@NgModule({
	declarations: [
		AppComponent,
		...PAGES,
	],
	imports: [
		ComponentsModule,
		BrowserModule,
		HttpClientModule,
		AppRoutingModule
	],
	providers: [...PROVIDERS],
	bootstrap: [AppComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
